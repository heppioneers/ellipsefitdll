
#include "minimiseMinuitVerbosity.h"

int minimiseMinuitVerbosity() {
		
	int print_control = MINUIT_SILENT;
	
	mnexcm_(dummyChiSq,"SET PRI", &print_control, &c__1, &nul, &futil, 7L);
	
	return 0;
}