
#include"ellipseFitChiSq.h"

/**
 * MINUIT function signature for chiSquared minimisation
 * calculates five parameter ellipse
 *
 * global constants are extern and referred to in header file:
 * int nData, array pointers dataX, dataY
 *
 * xval[0] = rotation angle in radians: alpha
 * xval[1] = semi-major axis magnitude: a
 * xval[2] = semi-minor axis magnitude: b
 * xval[3] = x of centre: cx
 * xval[4] = y of centre: cy
 */

int ellipseFitChiSq(int *npar, double *grad, double *fval, double *xval, int *iflag) {

        int i;
		
		double sqDiff;
		double chiSq = 0.0;
		
		double u;
		double uSq;
		
		double v;
        double vSq;
		
		double pSq;		
		double rSq;
		
		const double alpha = xval[0];
		
		const double sinAlpha = sin(alpha);
		const double sinSqAlpha = sinAlpha * sinAlpha;
		
		const double sinTwoAlpha = sin(alpha+alpha);
				
        const double aSq = xval[1] * xval[1];		
		const double bSq = xval[2] * xval[2];		
		
        for(i=nData;i--;) {
			
			u = yData[i] - xval[4];			
			v = xData[i] - xval[3];
			
			uSq = u*u;
			vSq = v*v;

            rSq	= uSq + vSq;
			
            pSq = bSq + (aSq - bSq) * ((uSq + sinSqAlpha*(vSq - uSq)) - (sinTwoAlpha*u*v));
			
			sqDiff = rSq + pSq - 2.0 * sqrt(rSq * pSq);
			
			chiSq += sqDiff;
		}

		fval[0] = chiSq;
		
		if(EXTRACT_PARAMS_IFLAG == iflag[0]) {
			
			for(i=5;i--;) {
                fitPar[i] = xval[i];
			}
		}
		
        return 0;
}

