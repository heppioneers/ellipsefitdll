
#include "ellipseFitEntryPoint.h"

int fitEllipseToXY(double *x, double *y, const int nPairs, double *fitParams) {    

	ellipseFitParameterConnection(fitParams);		
	ellipseFitXynDataTransfer(x,y,nPairs);
	
    ellipseFitMinuitInitialise();
	ellipseFitMinuitControl();
			
	return 0;
}