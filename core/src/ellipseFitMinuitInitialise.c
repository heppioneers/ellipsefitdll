
#include"ellipseFitMinuitInitialise.h"

int ellipseFitMinuitInitialise() {
		
	const double alphaSeed = 0.3;
	const double alphaSeedTol = 0.05;
	
	const double aSeed = 100.0;
	const double aSeedTol = 10.0;
	
	const double bSeed = 50.0;
	const double bSeedTol = 5.0;
	
	double cXSeed = 0.0;
	double cYSeed = 0.0;
	
	const double nPairs = (double)nData;
	const double rootN = sqrt(nPairs);
	
	const double cXTol = cXSeed / rootN;
	const double cYTol = cYSeed / rootN;
	
	seedCentre(nData,xData,&cXSeed);
	seedCentre(nData,yData,&cYSeed);
	
    initialiseMinuit();

    minimiseMinuitVerbosity();	
	
	mnparm_(&c__1,angleName,&alphaSeed,&alphaSeedTol,&dr__0,&dr__0,&nul,strlen(angleName));
	mnparm_(&c__2,semiMajorName,&aSeed,&aSeedTol,&dr__0,&dr__0,&nul,strlen(semiMajorName));
	mnparm_(&c__3,semiMinorName,&aSeed,&aSeedTol,&dr__0,&dr__0,&nul,strlen(semiMinorName));
	mnparm_(&c__4,centreXName,&cXSeed,&cXTol,&dr__0,&dr__0,&nul,strlen(centreXName));
	mnparm_(&c__5,centreYName,&cYSeed,&cYTol,&dr__0,&dr__0,&nul,strlen(centreYName));
	
	return 0;
}

