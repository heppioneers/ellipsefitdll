
#include"ellipseFitMinuitDrive.h"

int ellipseFitMinuitControl() {
	
	mncomd_(ellipseFitChiSq,"FIX 4 5", &nul, &futil,7L);
	mncomd_(ellipseFitChiSq,"MINI", &nul, &futil,4L);
    
	mncomd_(ellipseFitChiSq,"RESTORE", &nul, &futil,7L);
	mncomd_(ellipseFitChiSq,"MIGRA", &nul, &futil,7L);
	
	mncomd_(ellipseFitChiSq,EXTRACT_PARAMS_CALI, &nul, &futil,6L);	

    return 0;	
}
