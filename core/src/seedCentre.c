
#include"seedCentre.h"

int seedCentre(const double n, double *x, double *cX) {
	
	int i;
	double sum = 0.0;

	if(n < 1.0) {
		cX[0] = sum;
		return -1;
	}	
	
	if(n < 2.0) {
		
		cX[0] = x[0];
		return 0;
	}
	
	for(i=(int)n; i--;) { 
	    sum+=x[i];		
	}
	
	cX[0] = sum / n;
	
	return 0;
}
