
#ifndef _TOKEN_FOR_HEADER_ELLIPSE_FIT_CHI_SQU_AUG_2016_

#define _TOKEN_FOR_HEADER_ELLIPSE_FIT_CHI_SQU_AUG_2016_

#include<math.h>

#include"xynDataRefs.h"
#include"fitParRefs.h"

#include"minuitInteractionConstants.h"

int ellipseFitChiSq(int *npar, double *grad, double *fval, double *xval, int *iflag);

#endif