
#ifndef _ELLIPSE_FIT_MINUIT_PARAMETER_NAMES_AUG_2016_

#define _ELLIPSE_FIT_MINUIT_PARAMETER_NAMES_AUG_2016_

static char angleName[4] = "ALF";
static char semiMajorName[4]="a  ";
static char semiMinorName[4]="b  ";
static char centreXName[4]="cx ";
static char centreYName[4]="cy ";

#endif