
#ifndef _TOKEN_FOR_HEADER_ELLIPSE_FIT_XYN_DATA_TRANSFER_AUG_2016_

#define _TOKEN_FOR_HEADER_ELLIPSE_FIT_XYN_DATA_TRANSFER_AUG_2016_

int nData;

double* xData;
double* yData;

int ellipseFitXynDataTransfer(double *x, double *y, const int nPairs);

#endif
