
#ifndef _TOKEN_FOR_ELLIPSE_FITTING_DLL_ENTRYPOINT_JUL_2016

#define _TOKEN_FOR_ELLIPSE_FITTING_DLL_ENTRYPOINT_JUL_2016


#include"ellipseFitParameterConnection.h"
#include"ellipseFitXynDataTransfer.h"
#include"ellipseFitMinuitInitialise.h"
#include"ellipseFitMinuitDrive.h"

int fitEllipseToXY(double *x, double *y, const int nPairs, double *fitParams);

#endif