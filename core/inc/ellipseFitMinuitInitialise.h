
#ifndef _ELLIPSE_FIT_MINUIT_INITIALISE_AUG_2016_

#define _ELLIPSE_FIT_MINUIT_INITIALISE_AUG_2016_

#include<math.h>

#include"xynDataRefs.h"
#include"ellipseParameterNames.h"

#include"seedCentre.h"

#include"minuitInteractionConstants.h"
#include"minuit.h"

#include"minimiseMinuitVerbosity.h"
#include"initialiseMinuit.h"

int ellipseFitMinuitInitialise();

#endif