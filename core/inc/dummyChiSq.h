
#ifndef _DUMMY_CHI_SQUARED_TOKEN_AUG_2016_

#define _DUMMY_CHI_SQUARED_TOKEN_AUG_2016_

int dummyChiSq(int *npar, double *grad, double *fval, double *xval, int *iflag);

#endif