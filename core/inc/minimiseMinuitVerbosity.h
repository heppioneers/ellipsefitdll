
#ifndef _MINUIT_VERBOSITY_AUG_2016_

#define _MINUIT_VERBOSITY_AUG_2016_

#include"dummyChiSq.h"
#include"minuitInteractionConstants.h"

#include"minuit.h"

#define MINUIT_SILENT -1
#define MINUIT_QUIET 0
#define MINUIT_VERBOSE 1

int minimiseMinuitVerbosity();

#endif